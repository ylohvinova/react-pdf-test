import React from 'react';
import { Page, Text, View, Document, StyleSheet,  Image, PDFDownloadLink } from '@react-pdf/renderer';

// Create styles
const styles = StyleSheet.create({
  page: {
    flexDirection: 'row',
    backgroundColor: '#E4E4E4'
  },
  section: {
    margin: 10,
    padding: 10,
    flexGrow: 1
  },
  image: {
    width: "150px",
    height: "150px",
    padding: 10
  },
});

export const DocumentExample = () => (
    <Document>
      <Page size="A4" style={styles.page}>
        <View>
          <Image style={styles.image} src="/logo512.png" />
          <Text>
            Image here
          </Text>
        </View>
        <View style={styles.section}>
          <Text>Section #1</Text>
        </View>
        <View style={styles.section}>
          <Text>Section #2</Text>
        </View>
      </Page>
    </Document>
  )

// Create Document Component
export const DownloadButton = () => (
  <PDFDownloadLink document={<DocumentExample />} fileName="somename.pdf">
    {({ blob, url, loading, error }) => (loading ? 'Loading document...' : 'Download now!')}
  </PDFDownloadLink>
);
