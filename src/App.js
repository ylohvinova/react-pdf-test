import React from 'react';
import { PDFViewer } from '@react-pdf/renderer';

import './App.css';
import {DocumentExample, DownloadButton} from './components/Document';

function App() {
  return (
    <>
      <PDFViewer>
        <DocumentExample />
      </PDFViewer>
      <br/>
      <DownloadButton/>
    </>
  );
}

export default App;
